This is a CUDA kernel implementation of the RAN.

To compile, run ./make.sh

To use:

> from ran_layer import RANLayer
>
> ran = RANLayer(input_size, output_size, num_layers=1, bias=True, batch_first=True, bidirectional=False, recurrent_dropout_prob=0, use_hidden_for_gates=True, highway=False, tie_weights=False, use_tanh=True)
