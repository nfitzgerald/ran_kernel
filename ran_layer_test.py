import torch
from torch.autograd import Variable
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence

from unittest import TestCase
import pytest

from baseline.ran import RAN
from ran_layer import RANLayer
from baseline.measurements import Timer


class TestCustomRANLayer(TestCase):

    #def get_models_and_inputs(self, batch_size, input_size,
    #                          output_size, num_layers, timesteps, dropout_prob,
    #                          bidirectional, use_hidden_for_gates, highway,
    #                          tie_weights, use_tanh):
    def test_single_cell_model(self):
        args = self.get_models_and_inputs(1, 5, 11, 1, 1, 0.0, False, True, False, True, False, True, True)
        self.forward_and_backward_outputs_match(*args)

    def test_single_cell_model_eval(self):
        args = self.get_models_and_inputs(1, 5, 11, 1, 1, 0.0, False, True, False, True, False, True, False)
        self.forward_and_backward_outputs_match(*args)

    def test_single_cell_bidir_model(self):
        args = self.get_models_and_inputs(1, 5, 11, 1, 1, 0.0, True, True, False, True, False, True, True)
        self.forward_and_backward_outputs_match(*args)

    def test_tiny_2layer_model(self):
        args = self.get_models_and_inputs(1, 5, 11, 2, 1, 0.0, False, True, False, True, False, True, True)
        self.forward_and_backward_outputs_match(*args)

    def test_tiny_2timestep_model(self):
        args = self.get_models_and_inputs(1, 5, 11, 1, 2, 0.0, False, True, False, True, False, True, True)
        self.forward_and_backward_outputs_match(*args)

    def test_tiny_2batch_model(self):
        args = self.get_models_and_inputs(2, 5, 11, 1, 1, 0.0, False, True, False, True, False, True, True)
        self.forward_and_backward_outputs_match(*args)

    def test_tiny_bidir_2layer_model(self):
        args = self.get_models_and_inputs(1, 5, 11, 2, 1, 0.0, True, True, False, True, False, True, True)
        self.forward_and_backward_outputs_match(*args)

    def test_tiny_bidir_2timestep_model(self):
        args = self.get_models_and_inputs(1, 5, 11, 1, 2, 0.0, True, True, False, True, False, True, True)
        self.forward_and_backward_outputs_match(*args)

    def test_tiny_bidir_2batch_model(self):
        args = self.get_models_and_inputs(2, 5, 11, 1, 1, 0.0, True, True, False, True, False, True, True)
        self.forward_and_backward_outputs_match(*args)

    def test_small_model_base(self):
        args = self.get_models_and_inputs(5, 3, 11, 2, 5, 0.0, True, True, False, False, False, True, True)
        self.forward_and_backward_outputs_match(*args)

    def test_small_model_unidirectional(self):
        args = self.get_models_and_inputs(5, 3, 11, 2, 5, 0.0, False, True, False, False, False, True, True)
        self.forward_and_backward_outputs_match(*args)

    def test_small_model_no_tanh(self):
        args = self.get_models_and_inputs(5, 3, 11, 2, 5, 0.0, True, True, False, False, False, False, True)
        self.forward_and_backward_outputs_match(*args)

    def test_small_model_with_dropout_only(self):
        args = self.get_models_and_inputs(5, 3, 11, 2, 5, 0.5, True, True, False, False, False, True, True)
        self.forward_and_backward_outputs_match(*args)

    def test_small_model_with_dropout_and_highway(self):
        args = self.get_models_and_inputs(5, 3, 11, 2, 5, 0.0, True, True, False, True, False, True, True)
        self.forward_and_backward_outputs_match(*args)

    def test_small_model_with_tied_weights(self):
        args = self.get_models_and_inputs(5, 3, 11, 2, 5, 0.5, True, True, False, False, True, True, True)
        self.forward_and_backward_outputs_match(*args)

    def test_small_model_with_no_hidden(self):
        args = self.get_models_and_inputs(5, 3, 11, 2, 5, 0.0, True, False, False, True, False, True, True)
        self.forward_and_backward_outputs_match(*args)

    def test_small_model_with_tanh(self):
        args = self.get_models_and_inputs(5, 3, 11, 2, 5, 0.0, False, True, False, False, False, True, True)
        self.forward_and_backward_outputs_match(*args)

    def test_small_model_no_hidden_highway_tie_weights(self):
        args = self.get_models_and_inputs(5, 3, 11, 2, 7, 0.5, False, False, False, True, True, False, True)
        self.forward_and_backward_outputs_match(*args)

    #def get_models_and_inputs(self, batch_size, input_size,
    #                          output_size, num_layers, timesteps, dropout_prob,
    #                          bidirectional, use_hidden_for_gates, highway,
    #                          tie_weights, use_tanh):

    def test_large_model_base(self):
        args = self.get_models_and_inputs(83, 103, 311, 8, 101, 0.5, False, True, False, False, False, True, True)
        self.forward_and_backward_outputs_match(*args)

    def test_large_model_with_output_gate(self):
        args = self.get_models_and_inputs(83, 103, 311, 8, 101, 0.5, False, True, True, False, False, True, True)
        self.forward_and_backward_outputs_match(*args)

    def test_large_model_eval_base(self):
        args = self.get_models_and_inputs(83, 103, 311, 8, 101, 0.5, False, True, False, False, False, True, False)
        self.forward_and_backward_outputs_match(*args)

    def test_large_model_no_hidden_highway_tie_weights(self):
        args = self.get_models_and_inputs(83, 103, 311, 8, 101, 0.5, False, False, False, True, True, False, True)
        self.forward_and_backward_outputs_match(*args)

    def forward_and_backward_outputs_match(self, baseline_model, kernel_model,
                                           baseline_input, kernel_input, baseline_hidden, kernel_hidden, dropout):
        with Timer('Baseline'):
            baseline_output, baseline_hidden = baseline_model(baseline_input, hx=baseline_hidden, dropout_weights=dropout)
            baseline_output, _ = pad_packed_sequence(baseline_output, batch_first=True)

        with Timer("Mine"):
            kernel_output, kernel_hidden = kernel_model(kernel_input, hx=kernel_hidden, dropout_weights=dropout)
            kernel_output, _ = pad_packed_sequence(kernel_output, batch_first=True)

        diff = torch.max(baseline_output.data - kernel_output.data)
        assert diff < 1e-4, "Output does not match: " + str(diff)

        # Backprop some random error.
        back_err = torch.randn(baseline_output.size()).cuda()
        back_h_err = torch.randn(baseline_hidden[0].size()).cuda()
        back_c_err = torch.randn(baseline_hidden[0].size()).cuda()
        baseline_model.zero_grad()
        baseline_output.backward(back_err, retain_graph=True)
        baseline_hidden[0].backward(back_h_err, retain_graph=True)
        baseline_hidden[1].backward(back_c_err)

        kernel_model.zero_grad()
        kernel_output.backward(back_err, retain_graph=True)
        kernel_hidden[0].backward(back_h_err, retain_graph=True)
        kernel_hidden[1].backward(back_c_err)
        input_grad_diff = torch.max(self.baseline_input.grad.data - self.mine_input.grad.data)
        assert input_grad_diff < 1e-4, "Input grad does not match: " + str(input_grad_diff)


        weight_ind = 0
        bias_ind = 0
        for layer in range(baseline_model.num_layers):
            for dir in range(baseline_model.directions):
                layer_name = "layer_%d%s"%(layer, "_reverse" if dir == 1 else "")
                x_grad = getattr(baseline_model, layer_name).xlin.weight.grad
                if kernel_model.h_gates > 0:
                    h_grad = getattr(baseline_model, layer_name).hlin.weight.grad
                else:
                    h_grad = None
                bias = getattr(baseline_model, layer_name).bias.grad

                mine_x_grad = kernel_model.weight.grad[weight_ind:weight_ind+x_grad.nelement()].view(x_grad.size(1), x_grad.size(0)).t()
                weight_ind += x_grad.nelement()

                if h_grad is not None:
                    mine_h_grad = kernel_model.weight.grad[weight_ind:weight_ind+h_grad.nelement()].view(h_grad.size(1), h_grad.size(0)).t()
                    weight_ind += h_grad.nelement()

                mine_bias = kernel_model.bias.grad[bias_ind:bias_ind+bias.nelement()]
                bias_ind += bias.nelement()

                x_diff = torch.max(mine_x_grad.data - x_grad.data)
                assert x_diff < 1e-4, "%s x_weight does not match: " % layer_name + str(x_diff)

                if h_grad is not None:
                    h_diff = torch.max(mine_h_grad.data - h_grad.data)
                    assert h_diff < 1e-4, "%s h_weight does not match: " % layer_name + str(h_diff)

                bias_diff = torch.max(mine_bias.data - bias.data)
                assert bias_diff < 1e-4, "%s bias does not match: " % layer_name + str(bias_diff)

    def get_models_and_inputs(self, batch_size, input_size,
                              output_size, num_layers, timesteps, dropout_prob,
                              bidirectional, use_hidden_for_gates, output_gate, highway,
                              tie_weights, use_tanh, train):

        baseline = RAN(input_size,
                                   output_size,
                                   num_layers,
                                   recurrent_dropout_prob=dropout_prob,
                                   bidirectional=bidirectional,
                                   use_hidden_for_gates=use_hidden_for_gates,
                                   output_gate = output_gate,
                                   highway=highway,
                                   tie_weights=tie_weights,
                                   use_tanh=use_tanh).cuda()
        kernel_version = RANLayer(input_size,
                                  output_size,
                                  num_layers=num_layers,
                                  bidirectional=bidirectional,
                                  use_hidden_for_gates=use_hidden_for_gates,
                                  output_gate = output_gate,
                                  highway=highway,
                                  tie_weights=tie_weights,
                                  use_tanh=use_tanh,
                                  recurrent_dropout_prob=dropout_prob).cuda()
        directions = 2 if bidirectional else 1
        curr_weight_ind = 0
        curr_bias_ind = 0
        print("CUDA lstm - weight elements: ", kernel_version.weight.nelement())
        for layer in range(num_layers):
            for d in range(directions):
                print("Layer: ", layer, "Direction: ", d, "Weight Index: ", curr_weight_ind)
                print("bias index: ", curr_bias_ind)
                w_suffix = "_reverse" if d == 1 else ""
                x_weight = getattr(baseline, 'layer_%d%s' % (layer, w_suffix)).xlin.weight
                bias = getattr(baseline, 'layer_%d%s' % (layer, w_suffix)).bias
                kernel_version.weight.data[curr_weight_ind:curr_weight_ind+x_weight.nelement()].view(x_weight.size(1), x_weight.size(0)).copy_(x_weight.data.t())
                curr_weight_ind += x_weight.nelement()
                if use_hidden_for_gates:
                    h_weight = getattr(baseline, 'layer_%d%s' % (layer, w_suffix)).hlin.weight
                    kernel_version.weight.data[curr_weight_ind:curr_weight_ind+h_weight.nelement()].view(h_weight.size(1), h_weight.size(0)).copy_(h_weight.data.t())
                    curr_weight_ind += h_weight.nelement()
                kernel_version.bias.data[curr_bias_ind:curr_bias_ind+bias.nelement()].copy_(bias.data)
                curr_bias_ind += bias.nelement()

        input = torch.randn(batch_size, timesteps, input_size).cuda()
        input2 = input.clone()
        self.baseline_input = Variable(input, requires_grad=True)
        self.mine_input = Variable(input2, requires_grad=True)
        lengths = [timesteps - (i / 2) for i in range(batch_size)]
        lengths = lengths[:batch_size]
        baseline_input = pack_padded_sequence(self.baseline_input, lengths, batch_first=True)
        kernel_version_input = pack_padded_sequence(self.mine_input, lengths, batch_first=True)
        if dropout_prob > 0 and train:
            dropout = Variable(torch.Tensor(num_layers,
                                            directions,
                                            batch_size,
                                            output_size).cuda().bernoulli_(dropout_prob))
        else:
            dropout = None

        baseline_h0 = torch.randn(directions*num_layers, batch_size, output_size).cuda()
        baseline_c0 = torch.randn(directions*num_layers, batch_size, output_size).cuda()
        kernel_h0 = baseline_h0.clone()
        kernel_c0 = baseline_c0.clone()
        baseline_hidden = (Variable(baseline_h0), Variable(baseline_c0))
        kernel_hidden = (Variable(kernel_h0), Variable(kernel_c0))

        if train:
            baseline.train()
            kernel_version.train()
        else:
            baseline.eval()
            kernel_version.eval()

        return baseline, kernel_version, baseline_input, kernel_version_input, baseline_hidden, kernel_hidden, dropout
