#include "cuda_runtime.h"
#include "curand.h"
#include "cublas_v2.h"
#include <iostream>

#ifdef __cplusplus
extern "C" {
#endif

#include <float.h>
#include <stdio.h>
#include "ran_kernel.h"

#define BLOCK 256

// Define some error checking macros.
#define cudaErrCheck(stat) { cudaErrCheck_((stat), __FILE__, __LINE__); }
void cudaErrCheck_(cudaError_t stat, const char *file, int line) {
   if (stat != cudaSuccess) {
      fprintf(stderr, "CUDA Error: %s %s %d\n", cudaGetErrorString(stat), file, line);
   }
}

#define cublasErrCheck(stat) { cublasErrCheck_((stat), __FILE__, __LINE__); }
void cublasErrCheck_(cublasStatus_t stat, const char *file, int line) {
   if (stat != CUBLAS_STATUS_SUCCESS) {
      fprintf(stderr, "cuBLAS Error: %d %s %d\n", stat, file, line);
   }
}

// Device functions
__forceinline__ __device__ float sigmoidf(float in) {
   return 1.f / (1.f + expf(-in));  
}

__forceinline__ __device__ float dsigmoidf(float in) {
   float s = sigmoidf(in);
   return s * (1.f - s);
}

__forceinline__ __device__ float tanh2f(float in) {
   float t = tanhf(in);
   return t*t;
}

__global__ void elementWise_bp(int hiddenSize, int miniBatch, int numCovered,
                               int dir,
                               // Inputs
                               float *out_grad,
                               float *h_forward_grad,
                               float *c_forward_grad,
                               float *c_in,
                               float *c_forward,
                               float *h_forward,
                               float *gates_out,
                               float *dropout_in,
                               // Outputs
                               float *c_in_grad,
                               float *i_gates_grad,
                               float *h_gates_grad,
                               int training,
                               int use_hidden_for_gates,
                               int use_output_gate,
                               int highway,
                               int tie_weights,
                               int use_tanh,
                               int numDirections,
                               int i_gates,
                               int h_gates,
                               int bias_gates) {
 
   int index = blockIdx.x * blockDim.x + threadIdx.x;
   
   if (index >= numCovered * hiddenSize) return;
    
   int batch = index / hiddenSize;
   int h_gateIndex = (index % hiddenSize) + bias_gates * batch * hiddenSize;
   int i_gateIndex = (index % hiddenSize) + i_gates * batch * hiddenSize;   

   float d_h = out_grad[(index % hiddenSize) + dir * hiddenSize + batch * numDirections * hiddenSize] + h_forward_grad[index];
   d_h = d_h * dropout_in[index];

   float c_tmp = gates_out[i_gateIndex];
   float in_gate = gates_out[i_gateIndex + 1 * hiddenSize];
   int i_ind = 2;

   float forget_gate = NULL;
   if (tie_weights != 1) {
       forget_gate = gates_out[i_gateIndex + i_ind++ * hiddenSize];
   } else {
       forget_gate = 1.f - in_gate;
   }

   float output_gate;
   if (use_output_gate == 1){
       output_gate = gates_out[i_gateIndex + i_ind++ * hiddenSize];
   } else {
       output_gate = 1.f;
   }

   float r_gate = NULL;
   float lin_gate = NULL;
   if (highway == 1) {
       r_gate = gates_out[i_gateIndex + i_ind++ * hiddenSize];
       lin_gate = gates_out[i_gateIndex + i_ind++ * hiddenSize];
   }

   float d_out;
   if (highway == 1) {
      d_out = d_h * r_gate;   
   } else {
      d_out = d_h;
   }

   float d_c_tmp;
   float d_in_gate;
   float d_forget_gate;
   float d_output_gate;
   float d_r_gate;
   float d_lin_gate;

   float h_prime;
   if (use_tanh == 1) {
       h_prime = tanhf(c_forward[index]);
   } else {
       h_prime = c_forward[index];
   }

   if (highway == 1) {
       d_r_gate = d_h * (h_prime - lin_gate) * r_gate * (1.f - r_gate);
       d_lin_gate = d_h * (1.f - r_gate);
   }

   if (use_output_gate == 1) {
        d_output_gate = d_out * h_prime * output_gate * (1.f - output_gate);
   }

   float d_c;
   if (use_tanh == 1) {
     d_c = d_out * output_gate * (1.f - tanh2f(c_forward[index])) + c_forward_grad[index];
   } else {
     d_c = d_out* output_gate + c_forward_grad[index];
   }

   if (tie_weights) {
        d_in_gate = d_c * (c_tmp - c_in[index]) * in_gate * (1.f - in_gate);
   } else {
       d_in_gate = d_c * c_tmp * in_gate * (1.f - in_gate);
       d_forget_gate = d_c * c_in[index] * forget_gate * (1.f - forget_gate);
   }

   d_c_tmp = d_c * in_gate;

   i_ind = 0;
   int h_ind = 0;
   i_gates_grad[i_gateIndex + i_ind++ * hiddenSize] = d_c_tmp;
   i_gates_grad[i_gateIndex + i_ind++ * hiddenSize] = d_in_gate;
   h_gates_grad[h_gateIndex + h_ind++ * hiddenSize] = d_in_gate;

   if (tie_weights != 1) {
       i_gates_grad[i_gateIndex + i_ind++ * hiddenSize] = d_forget_gate;
       h_gates_grad[h_gateIndex + h_ind++ * hiddenSize] = d_forget_gate;
   }

   if (use_output_gate == 1) {
       i_gates_grad[i_gateIndex + i_ind++ * hiddenSize] = d_output_gate;
       h_gates_grad[h_gateIndex + h_ind++ * hiddenSize] = d_output_gate;
   }

   if (highway == 1) {
       i_gates_grad[i_gateIndex + i_ind++ * hiddenSize] = d_r_gate;
       i_gates_grad[i_gateIndex + i_ind++ * hiddenSize] = d_lin_gate;
       h_gates_grad[h_gateIndex + h_ind++ * hiddenSize] = d_r_gate;
   }

   c_in_grad[index] = forget_gate * d_c;
}


// Fused forward kernel
__global__ void elementWise_fp(int hiddenSize, int miniBatch, int numCovered,
                               int dir,
                               float *tmp_h, 
                               float *tmp_i, 
                               float *bias,
                               float *linearGates,
                               float *h_forward,
                               float *dropout_in,
                               float *c_in,
                               float *h_out,
                               float *c_out,
                               int training,
                               int use_hidden_for_gates,
                               int use_output_gate,
                               int highway,
                               int tie_weights,
                               int use_tanh,
                               int numDirections,
                               int i_gates,
                               int h_gates,
                               int bias_gates) {
   int index = blockIdx.x * blockDim.x + threadIdx.x;

   if (index >= numCovered * hiddenSize) return;

   
   int batch = index / hiddenSize;
   int h_gateIndex = (index % hiddenSize) + h_gates * batch * hiddenSize;
   int i_gateIndex = (index % hiddenSize) + i_gates * batch * hiddenSize;   

   int i_ind = 0;
   int h_ind = 0;
   int bias_ind = 0;

   float c_tmp = tmp_i[i_gateIndex];
   i_ind++;
   linearGates[i_gateIndex] = c_tmp;

   float i = tmp_i[i_ind * hiddenSize + i_gateIndex] + bias[bias_ind++ * hiddenSize + index % hiddenSize];
   if (use_hidden_for_gates == 1) {
     i += tmp_h[h_ind * hiddenSize + h_gateIndex];
     h_ind++;
   }
   i = sigmoidf(i);
   linearGates[i_ind * hiddenSize + i_gateIndex] = i;
   i_ind++;

   float f;
   if (tie_weights == 1) {
       f = 1 - i;
   } else {
       f = tmp_i[i_ind * hiddenSize + i_gateIndex] + bias[bias_ind++ * hiddenSize + index % hiddenSize];
       if (use_hidden_for_gates == 1) {
           f += tmp_h[h_ind * hiddenSize + h_gateIndex];
           h_ind++;
       }
       f = sigmoidf(f);
       linearGates[i_ind * hiddenSize + i_gateIndex] = f;
       i_ind++;
   }


   float c = i * c_tmp + f * c_in[index];

   float h;
   if (use_tanh == 1) {
       h = tanhf(c);
   } else {
       h = c;
   }

   if (use_output_gate == 1) {
       float o = tmp_i[i_ind * hiddenSize + i_gateIndex] + bias[bias_ind++ * hiddenSize + index % hiddenSize];
       if (use_hidden_for_gates == 1) {
          o += tmp_h[h_ind * hiddenSize + h_gateIndex];
          h_ind++;
       }
       o = sigmoidf(o);
       linearGates[i_ind * hiddenSize + i_gateIndex] = o;
       i_ind++;
       h = o * h;
   }

   if (highway == 1) {
       float r = tmp_i[i_ind * hiddenSize + i_gateIndex] + bias[bias_ind++ * hiddenSize + index % hiddenSize];
       if (use_hidden_for_gates == 1) {
           r += tmp_h[h_ind * hiddenSize + h_gateIndex];
           h_ind++;
       }
       r = sigmoidf(r);
       linearGates[i_ind * hiddenSize + i_gateIndex] = r;
       i_ind++;

       float lin = tmp_i[i_ind * hiddenSize + i_gateIndex];
       linearGates[i_ind * hiddenSize + i_gateIndex] = lin;
       i_ind++;
       h = r * h + (1 - r) * lin;
   }

   h = h * dropout_in[index];

   c_out[index] = c;
   h_forward[index] = h;
   h_out[(index%hiddenSize) + batch * numDirections * hiddenSize + dir * hiddenSize] = h;
}

void ran_backward_ongpu(int inputSize, int hiddenSize, int miniBatch,
        int numLayers, int seqLength, float *out_grad, int *lengths,
        float *h_data_grad, float * c_data_grad, float *x, float* h_out, float *h_data,
        float *c_data, float *T,
        float *gates_out, float *dropout_in, float *h_gates_grad,
        float *i_gates_grad, float *h_out_grad, float *x_grad, float *T_grad, float *bias_grad,
        int isTraining, int do_weight_grad, int use_hidden_for_gates, int use_output_gate, int highway, int weight_tie,
        int use_tanh, int numDirections, int i_gates, int h_gates, int bias_gates, 
        cudaStream_t stream, cublasHandle_t handle) {


    const int numElements = hiddenSize * miniBatch;

    cudaStream_t stream_i;
    cudaStream_t stream_h;
    cudaStream_t stream_wi;
    cudaStream_t stream_wh;
    cudaStream_t stream_wb;

    cudaErrCheck(cudaStreamCreate(&stream_i));
    cudaErrCheck(cudaStreamCreate(&stream_h));
    cudaErrCheck(cudaStreamCreate(&stream_wi));
    cudaErrCheck(cudaStreamCreate(&stream_wh));
    cudaErrCheck(cudaStreamCreate(&stream_wb));

    float one = 1.f;
    float zero = 0.f;

    float *ones_host = new float[miniBatch];
    for (int i=0; i < miniBatch; i++) {
        ones_host[i] = 1.f;
    }
    float *ones;
    cudaErrCheck(cudaMalloc((void**)&ones, miniBatch * sizeof(float)));
    cudaErrCheck(cudaMemcpy(ones, ones_host, miniBatch * sizeof(float), cudaMemcpyHostToDevice));

    for (int layer = numLayers-1; layer >= 0; layer--) {
        for (int dir = 0; dir < numDirections; dir++) {
            int direction;
            int startInd;
            int currNumCovered;
            if (dir == 0) {
                // forward direction
                direction = -1;
                startInd = seqLength-1;
                currNumCovered = 0;
            } else {
                // backward direction
                direction = 1;
                startInd = 0;
                currNumCovered = miniBatch;
            }

            for (int t = startInd; t < seqLength && t >= 0; t = t + direction) {
                
                int prevIndex;
                int prevGradIndex;
                if (direction == 1) {
                    while (lengths[currNumCovered-1] <= t) {
                        currNumCovered--;
                    }
                    prevGradIndex = t;
                    prevIndex = (t+2)%(seqLength+1);
                } else {
                    while ((currNumCovered < miniBatch) && (lengths[currNumCovered] > t)) {
                        currNumCovered++;
                    }
                    prevGradIndex = (t+2)%(seqLength+1);
                    prevIndex = t;
                }


                float * gradPtr;
                if (layer == numLayers-1) {
                    gradPtr = out_grad + t * numDirections * numElements;
                } else {
                    gradPtr = h_out_grad + t * numDirections * numElements + layer * seqLength * numDirections * numElements;
                }

                cublasErrCheck(cublasSetStream(handle, stream_i));

                dim3 blockDim;
                dim3 gridDim;

                blockDim.x = BLOCK;
                gridDim.x = ((currNumCovered * hiddenSize) + blockDim.x - 1) / blockDim.x;               

                elementWise_bp <<< gridDim, blockDim , 0, stream>>> 
                    (hiddenSize, miniBatch, currNumCovered,
                     dir,
                     gradPtr,
                     h_data_grad + prevGradIndex * numElements + dir * (seqLength + 1) * numElements + layer * numDirections * (seqLength + 1) * numElements,
                     c_data_grad + prevGradIndex * numElements + dir * (seqLength + 1) * numElements + layer * numDirections * (seqLength + 1) * numElements,
                     c_data + prevIndex * numElements + dir * (seqLength+1) * numElements + layer * numDirections * (seqLength + 1) * numElements,
                     c_data + (t+1) * numElements + dir * (seqLength+1) * numElements + layer * numDirections * (seqLength + 1) * numElements,
                     h_data + (t+1) * numElements + dir * (seqLength+1) * numElements + layer * numDirections * (seqLength + 1) * numElements,
                     gates_out + t * i_gates * numElements + dir * seqLength * i_gates * numElements + layer * numDirections * seqLength * i_gates * numElements,
                     dropout_in + dir * numElements + layer * numDirections * numElements,
                     c_data_grad + (t+1) * numElements + dir * (seqLength+1) * numElements + layer * numDirections * (seqLength + 1) * numElements,
                     i_gates_grad + dir * i_gates * numElements,
                     h_gates_grad + dir * bias_gates * numElements,
                     isTraining,
                     use_hidden_for_gates, use_output_gate, highway,
                     weight_tie, use_tanh,
                     numDirections, i_gates, h_gates, bias_gates);
                   cudaErrCheck(cudaGetLastError());
                   // END

                 cudaErrCheck(cudaDeviceSynchronize());

                 float *out_grad_ptr;
                 int weightStart;
                 int inSize;
                 if (layer == 0) {
                     inSize = inputSize;
                     out_grad_ptr = x_grad + t * inputSize * miniBatch;
                     weightStart = dir * (i_gates * inputSize * hiddenSize + h_gates * hiddenSize * hiddenSize);
                 } else {
                     inSize = numDirections * hiddenSize;
                     out_grad_ptr = h_out_grad + t * numDirections * numElements + (layer-1) * seqLength * numDirections * numElements;
                    weightStart = numDirections * (i_gates * hiddenSize * inputSize + h_gates * hiddenSize * hiddenSize) + numDirections * (layer - 1) * (i_gates*numDirections*hiddenSize*hiddenSize + h_gates*hiddenSize*hiddenSize) + dir * (i_gates*numDirections*hiddenSize*hiddenSize + h_gates * hiddenSize * hiddenSize);
                 }

                 cublasErrCheck(cublasSgemm(handle,
                             CUBLAS_OP_T, CUBLAS_OP_N,
                             inSize, currNumCovered, i_gates*hiddenSize,
                             &one,
                             &T[weightStart],
                             i_gates * hiddenSize,
                             i_gates_grad + dir * i_gates * numElements,
                             i_gates * hiddenSize,
                             &one,
                             out_grad_ptr,
                             inSize));

                 cublasErrCheck(cublasSetStream(handle, stream_h));

                 if (use_hidden_for_gates == 1) {
                     cublasErrCheck(cublasSgemm(handle,
                                CUBLAS_OP_T, CUBLAS_OP_N,
                                hiddenSize, currNumCovered, h_gates*hiddenSize,
                                &one,
                                &T[weightStart + i_gates*hiddenSize*inSize],
                                h_gates * hiddenSize,
                                h_gates_grad + dir * bias_gates * numElements,
                                h_gates * hiddenSize,
                                &zero,
                                h_data_grad + (t+1) * numElements + dir * (seqLength+1) * numElements + layer * numDirections * (seqLength+1) * numElements,
                                hiddenSize));
                 }

                 if (do_weight_grad == 1) {
                     float *inputPtr;
                     if (layer == 0) {
                         inputPtr = x + t * inputSize * miniBatch;
                     } else {
                         inputPtr = h_out + t * numDirections * numElements + (layer - 1) * seqLength * numDirections * numElements;
                     }

                     cublasErrCheck(cublasSetStream(handle, stream_wi));

                     // Update i_weights
                     cublasErrCheck(cublasSgemm(handle,
                                 CUBLAS_OP_N, CUBLAS_OP_T,
                                 i_gates * hiddenSize, inSize, currNumCovered,
                                 &one,
                                 i_gates_grad + dir * i_gates * numElements,
                                 i_gates * hiddenSize,
                                 inputPtr,
                                 inSize,
                                 &one,
                                 &T_grad[weightStart],
                                 i_gates * hiddenSize));


                     if (use_hidden_for_gates == 1) {
                         cublasErrCheck(cublasSetStream(handle, stream_wh));
                         // Update h_weights
                         cublasErrCheck(cublasSgemm(handle,
                                     CUBLAS_OP_N, CUBLAS_OP_T,
                                     h_gates * hiddenSize, hiddenSize, currNumCovered,
                                     &one,
                                     h_gates_grad + dir * bias_gates * numElements,
                                     h_gates * hiddenSize,
                                     h_data + prevIndex * numElements + dir * (seqLength+1) * numElements + layer * numDirections * (seqLength+1) * numElements,
                                     hiddenSize,
                                     &one,
                                     &T_grad[weightStart + i_gates *hiddenSize*inSize],
                                     h_gates * hiddenSize));
                     }

                     cublasErrCheck(cublasSetStream(handle, stream_wb));

                     // Update bias_weights
                     cublasErrCheck(cublasSgemv(handle,
                                 CUBLAS_OP_N,
                                 bias_gates * hiddenSize, currNumCovered,
                                 &one,
                                 h_gates_grad + dir * bias_gates * numElements,
                                 bias_gates * hiddenSize,
                                 ones,
                                 1,
                                 &one,
                                 &bias_grad[dir * bias_gates * hiddenSize + layer * numDirections * bias_gates * hiddenSize],
                                 1));
                 }

               cudaErrCheck(cudaDeviceSynchronize());

            }
        }
   }

   cublasErrCheck(cublasSetStream(handle, stream));
   cudaErrCheck(cudaStreamDestroy(stream_i));
   cudaErrCheck(cudaStreamDestroy(stream_h));
   cudaErrCheck(cudaStreamDestroy(stream_wi));
   cudaErrCheck(cudaStreamDestroy(stream_wh));
   cudaErrCheck(cudaStreamDestroy(stream_wb));

   free(ones_host);

   cudaErrCheck(cudaDeviceSynchronize());
}

void ran_forward_ongpu(int inputSize, int hiddenSize, int miniBatch, 
        int numLayers, int seqLength, float *x, int *lengths, float *h_out, float *h_data, 
        float *c_data, float *tmp_i, float *tmp_h, float *T, float *bias,
        float *dropout, float *gates, int is_training, int use_hidden_for_gates, int use_output_gate, int highway, int weight_tie, int use_tanh, int numDirections, int i_gates, int h_gates, int bias_gates, cudaStream_t stream, cublasHandle_t handle) {

    const int numElements = hiddenSize * miniBatch;

    float zero = 0.f;
    float one = 1.f;

    cudaStream_t stream_i;
    cudaStream_t stream_h;

    cudaErrCheck(cudaStreamCreate(&stream_i));
    cudaErrCheck(cudaStreamCreate(&stream_h));

    for (int layer = 0; layer < numLayers; layer++) {
        for (int dir= 0; dir < numDirections; dir++) {
            int direction;
            int startInd;
            int currNumCovered;
            if (dir== 0) {
                // forward direction
                direction = 1;
                startInd = 0;
                currNumCovered = miniBatch;
            } else {
                // backward direction
                direction = -1;
                startInd = seqLength-1;
                currNumCovered = 0;
            }
            cublasErrCheck(cublasSetStream(handle, stream));

            for (int t = startInd; t < seqLength && t >= 0; t = t + direction) {
                
                int prevIndex;
                if (direction == 1) {
                    while (lengths[currNumCovered-1] <= t) {
                        currNumCovered--;
                    }
                    prevIndex = t;
                } else {
                    while ((currNumCovered < miniBatch) && (lengths[currNumCovered] > t)) {
                        currNumCovered++;
                    }
                    prevIndex = (t+2)%(seqLength+1);
                }

                int inSize;
                int weightStart;
                float *inputPtr;
                if (layer == 0) {
                    inSize = inputSize;
                    weightStart = dir * (i_gates * inputSize * hiddenSize + h_gates * hiddenSize * hiddenSize);
                    inputPtr = x + t * inputSize * miniBatch;
                } else {
                    inSize = numDirections * hiddenSize;
                    weightStart = numDirections * (i_gates * hiddenSize * inputSize + h_gates * hiddenSize * hiddenSize) + numDirections * (layer - 1) * (i_gates*numDirections*hiddenSize*hiddenSize + h_gates*hiddenSize*hiddenSize) + dir * (i_gates*numDirections*hiddenSize*hiddenSize + h_gates * hiddenSize * hiddenSize);
                    inputPtr = h_out + t * numDirections * numElements + (layer - 1) * seqLength * numDirections * numElements;
                }

                cublasErrCheck(cublasSetStream(handle, stream_i));

                cublasErrCheck(cublasSgemm(handle,
                            CUBLAS_OP_N, CUBLAS_OP_N,
                            i_gates*hiddenSize, currNumCovered, inSize,
                            &one,
                            &T[weightStart],
                            i_gates * hiddenSize,
                            inputPtr,
                            inSize,
                            &zero,
                            tmp_i + dir * i_gates * numElements,
                            i_gates * hiddenSize));

                if (h_gates > 0) {
                    cublasErrCheck(cublasSetStream(handle, stream_h));

                    cublasErrCheck(cublasSgemm(handle,
                                CUBLAS_OP_N, CUBLAS_OP_N,
                                h_gates*hiddenSize, currNumCovered, hiddenSize,
                                &one,
                                &T[i_gates * hiddenSize * inSize + weightStart],
                                h_gates * hiddenSize,
                                h_data + prevIndex * numElements + dir * (seqLength + 1) * numElements + layer * numDirections * (seqLength + 1) * numElements,
                                hiddenSize,
                                &zero,
                                tmp_h + dir * h_gates * numElements,
                                h_gates * hiddenSize));
                }

                cudaErrCheck(cudaDeviceSynchronize());

                dim3 blockDim;
                dim3 gridDim;

                blockDim.x = BLOCK;
                gridDim.x = ((currNumCovered * hiddenSize) + blockDim.x - 1) / blockDim.x;               
                elementWise_fp <<< gridDim, blockDim , 0, stream>>> 
                    (hiddenSize, miniBatch, currNumCovered, dir,
                     h_gates > 0 ? tmp_h + dir * h_gates * numElements : NULL, 
                     tmp_i + dir * i_gates * numElements, 
                     bias + layer * numDirections * bias_gates * hiddenSize + dir * bias_gates * hiddenSize,
                     gates + i_gates * (t * numElements + dir * seqLength * numElements + layer * numDirections * seqLength * numElements),
                     h_data + (t + 1) * numElements + dir * (seqLength + 1) * numElements+ layer * numDirections * (seqLength + 1) * numElements,
                     dropout + layer * numDirections * numElements + dir * numElements,
                     c_data + prevIndex * numElements + dir * (seqLength + 1) * numElements + layer * numDirections * (seqLength + 1) * numElements,
                     h_out + t * numDirections * numElements + layer * seqLength * numDirections * numElements,
                     c_data + (t + 1) * numElements + dir * (seqLength + 1) * numElements + layer * numDirections * (seqLength + 1) * numElements,
                     is_training,
                     use_hidden_for_gates, use_output_gate, highway, weight_tie, use_tanh,
                     numDirections, i_gates, h_gates, bias_gates);
                   cudaErrCheck(cudaGetLastError());

                cudaErrCheck(cudaDeviceSynchronize());
            }
        }
    }

   cublasErrCheck(cublasSetStream(handle, stream));
   cudaErrCheck(cudaStreamDestroy(stream_i));
   cudaErrCheck(cudaStreamDestroy(stream_h));

   cudaErrCheck(cudaDeviceSynchronize());
}

#ifdef __cplusplus
}
#endif
