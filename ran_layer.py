import torch
import numpy
from torch.autograd import Function, NestedIOFunction, Variable
from torch.nn import Parameter
from torch.nn.utils.rnn import PackedSequence, pad_packed_sequence, pack_padded_sequence
from _ext import ran_layer

class RANFunction(NestedIOFunction):
    def __init__(self, input_size, hidden_size, num_layers=1,
            recurrent_dropout_prob=0, train=True, use_hidden_for_gates=True, output_gate = False, highway=False, tie_weights=False, use_tanh=True, directions=1):
        super(RANFunction, self).__init__()
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.num_layers = num_layers
        self.train = train
        self.use_hidden_for_gates = use_hidden_for_gates
        self.output_gate = output_gate
        self.highway = highway
        self.tie_weights = tie_weights
        self.use_tanh = use_tanh
        self.num_directions = directions
        self.i_gates, self.h_gates, self.bias_gates = num_gates(output_gate, tie_weights, highway, use_hidden_for_gates)

    def forward_extended(self, input, weight, bias, hy, cy, dropout, lengths, gates, output):
        self.seq_length, self.mini_batch, self.input_size = input.size()

        tmp_i = input.new(self.num_directions, self.mini_batch, self.i_gates * self.hidden_size).zero_()
        tmp_h = input.new()
        if self.h_gates > 0:
            tmp_h.resize_(self.num_directions, self.mini_batch, self.h_gates * self.hidden_size)

        ran_layer.ran_forward_cuda(
                self.input_size, self.hidden_size, self.mini_batch, self.num_layers,
                self.seq_length, input, lengths, output, hy, cy, tmp_i, tmp_h, weight,
                bias, dropout, gates, 1 if self.train else 0, 1 if self.use_hidden_for_gates else 0, 1 if self.output_gate else 0, 1 if self.highway else 0, 1 if self.tie_weights else 0, 1 if self.use_tanh else 0, self.num_directions, self.i_gates, self.h_gates, self.bias_gates)

        self.save_for_backward(input, lengths, weight, bias, hy, cy, dropout, gates, output)

        if self.num_directions == 2:
            hx_out = torch.cat([hy[:, 0, self.seq_length].squeeze(), hy[:, 1, 0].squeeze()], 0)
            cx_out = torch.cat([cy[:, 0, self.seq_length].squeeze(), cy[:, 1, 0].squeeze()], 0)
        else:
            hx_out = hy[:, 0, self.seq_length]
            cx_out = cy[:, 0, self.seq_length]

        return output[-1], hy[:,:,1:], hx_out, cx_out

    def backward(self, grad_output, grad_hy, grad_hx_in, grad_cx_in):
        input, lengths, weight, bias, hy, cy, dropout, gates, output = self.saved_tensors
        input = input.contiguous()
        grad_input, grad_weight, grad_bias, grad_hx, grad_cx, grad_dropout, grad_lengths, grad_gates, grad_output_buf = None, None, None, None, None, None, None, None, None

        grad_input = input.new().resize_as_(input).zero_()
        grad_hx = input.new().resize_as_(hy).zero_()
        grad_cx = input.new().resize_as_(cy).zero_()
        grad_hx[:,:,0].copy_(grad_hx_in.view(self.num_layers, self.num_directions, self.mini_batch, self.hidden_size))
        grad_cx[:,:,0].copy_(grad_cx_in.view(self.num_layers, self.num_directions, self.mini_batch, self.hidden_size))

        grad_weight = input.new()
        grad_bias = input.new()
        if self.needs_input_grad[1]:
            grad_weight.resize_as_(weight).zero_()
            grad_bias.resize_as_(bias).zero_()

        tmp_i_gates_grad = input.new().resize_(self.num_directions, self.mini_batch, self.i_gates * self.hidden_size).zero_()
        tmp_h_gates_grad = input.new().resize_(self.num_directions, self.mini_batch, self.bias_gates * self.hidden_size).zero_()

        ran_layer.ran_backward_cuda(
                self.input_size, self.hidden_size, self.mini_batch, self.num_layers,
                self.seq_length, grad_output, lengths, grad_hx, grad_cx, input, output,
                hy, cy, weight, gates, dropout,
                tmp_h_gates_grad, tmp_i_gates_grad, grad_hy, grad_input,
                grad_weight, grad_bias, 1 if self.train else 0, 1 if self.needs_input_grad[1] else 0,
                1 if self.use_hidden_for_gates else 0, 1 if self.output_gate else 0, 1 if self.highway else 0, 1 if self.tie_weights else 0,
                1 if self.use_tanh else 0, self.num_directions, self.i_gates, self.h_gates, self.bias_gates)

        return grad_input, grad_weight, grad_bias, grad_hx, grad_cx, grad_dropout, grad_lengths, grad_gates, grad_output_buf

def num_gates(output_gate, tie_weights, highway, use_hidden_for_gates):
    i_gates = 2
    h_gates = 1
    if not tie_weights:
        i_gates += 1
        h_gates += 1
    if output_gate:
        i_gates += 1
        h_gates += 1
    if highway:
        i_gates += 2
        h_gates += 1
    bias_gates = h_gates

    if not use_hidden_for_gates:
        h_gates = 0

    return i_gates, h_gates, bias_gates

class RANLayer(torch.nn.Module):
    def __init__(self, input_size, hidden_size, num_layers=1, bias=True, batch_first=True, bidirectional=False, recurrent_dropout_prob=0, use_hidden_for_gates=True, output_gate = False, highway=False, tie_weights=False, use_tanh=True):
        super(RANLayer, self).__init__()
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.num_layers = num_layers
        self.bias = bias
        self.recurrent_dropout_prob = recurrent_dropout_prob
        self.batch_first = batch_first
        self.training = True
        self.directions = 2 if bidirectional else 1
        self.use_hidden_for_gates = use_hidden_for_gates
        self.output_gate = output_gate
        self.highway = highway
        self.tie_weights = tie_weights
        self.use_tanh = use_tanh

        i_gates, h_gates, bias_gates = num_gates(self.output_gate, self.tie_weights, self.highway, self.use_hidden_for_gates)
        self.i_gates = i_gates
        self.h_gates = h_gates
        self.bias_gates = bias_gates

        self.ih_size = i_gates * hidden_size
        self.hh_size = h_gates * hidden_size
        self.bias_size = bias_gates * hidden_size

        assert num_layers > 0

        weight_size = 0
        bias_size = 0
        for layer in range(num_layers):
            layer_input_size = input_size if layer == 0 else self.directions * hidden_size

            ih_weights = self.ih_size * layer_input_size
            hh_weights = self.hh_size * hidden_size
            weight_size += ih_weights + hh_weights
            
            if bias:
                bias_size += self.bias_size

        weight_size *= self.directions
        bias_size *= self.directions

        self.weight = Parameter(torch.FloatTensor(weight_size))
        if bias:
            self.bias = Parameter(torch.FloatTensor(bias_size))

        self.reset_parameters()

    def reset_parameters(self):
        self.bias.data.zero_()
        weight_index = 0
        bias_index = 0
        for i in range(self.num_layers):
            for d in range(self.directions):
                insize = self.input_size if i == 0 else self.hidden_size
                i_weights = block_orthonormal_initialization(insize, self.hidden_size, self.i_gates)
                self.weight.data[weight_index:weight_index + i_weights.nelement()].view_as(i_weights).copy_(i_weights)
                weight_index += i_weights.nelement()

                if self.h_gates > 0:
                    h_weights = block_orthonormal_initialization(self.hidden_size, self.hidden_size, self.h_gates)
                    self.weight.data[weight_index:weight_index + h_weights.nelement()].view_as(h_weights).copy_(h_weights)
                    weight_index += h_weights.nelement()

    def forward(self, input, hx=None, dropout_weights=None):

        assert isinstance(input, PackedSequence), "RANLayer only accepts PackedSequence input"
        input, lengths = pad_packed_sequence(input, batch_first = self.batch_first)

        if self.batch_first:
            input = input.transpose(0,1)

        self.seq_length, self.mini_batch, input_size = input.size()
        assert input_size == self.input_size

        if hx is not None:
            h0 = hx[0].view(self.num_layers, self.directions, 1, self.mini_batch, self.hidden_size)
            c0 = hx[1].view(self.num_layers, self.directions, 1, self.mini_batch, self.hidden_size)
        else:
            h0 = Variable(input.data.new(self.num_layers, self.directions, 1, self.mini_batch, self.hidden_size))
            c0 = Variable(input.data.new(self.num_layers, self.directions, 1, self.mini_batch, self.hidden_size))

        h_rest = Variable(input.data.new(self.num_layers, self.directions, self.seq_length, self.mini_batch, self.hidden_size).zero_(), requires_grad=False)
        c_rest = Variable(input.data.new(self.num_layers, self.directions, self.seq_length, self.mini_batch, self.hidden_size).zero_(), requires_grad=False)

        hy = torch.cat([h0, h_rest], 2)
        cy = torch.cat([c0, c_rest], 2)

        output = Variable(input.data.new(self.num_layers, self.seq_length, self.mini_batch, self.hidden_size * self.directions), requires_grad=False)

        if dropout_weights is None or not self.training:
            dropout_weights = input.data.new().resize_(self.num_layers, self.directions, self.mini_batch, self.hidden_size)
            if self.training:
                dropout_weights.bernoulli_(1 - self.recurrent_dropout_prob)
            else:
                dropout_weights.fill_(1 - self.recurrent_dropout_prob)
            dropout_weights = Variable(dropout_weights, requires_grad = False)

        gates = Variable(input.data.new().resize_(self.num_layers, self.directions, self.seq_length, self.mini_batch, self.i_gates * self.hidden_size))

        lengths_var = Variable(torch.IntTensor(lengths))

        output, hidden, h, c = RANFunction(self.input_size, self.hidden_size, num_layers=self.num_layers, directions=self.directions, train=self.training, use_hidden_for_gates=self.use_hidden_for_gates, output_gate = self.output_gate, highway=self.highway, tie_weights=self.tie_weights, use_tanh=self.use_tanh)(input, self.weight, self.bias, hy, cy, dropout_weights, lengths_var, gates, output)

        output = output.transpose(0,1)

        output = pack_padded_sequence(output, lengths, batch_first = self.batch_first)

        return output, (h, c)

def orthonormal_initialization(dim_in, dim_out, factor=1.0, seed=None, dtype='float64'):
    rng = numpy.random.RandomState(seed)
    if dim_in == dim_out:
        M = rng.randn(*[dim_in, dim_out]).astype(dtype)
        Q, R = numpy.linalg.qr(M)
        Q = Q * numpy.sign(numpy.diag(R))
        param = torch.Tensor(Q * factor)
    else:
        M1 = rng.randn(dim_in, dim_in).astype(dtype)
        M2 = rng.randn(dim_out, dim_out).astype(dtype)
        Q1, R1 = numpy.linalg.qr(M1)
        Q2, R2 = numpy.linalg.qr(M2)
        Q1 = Q1 * numpy.sign(numpy.diag(R1))
        Q2 = Q2 * numpy.sign(numpy.diag(R2))
        n_min = min(dim_in, dim_out)
        param = numpy.dot(Q1[:, :n_min], Q2[:n_min, :]) * factor
        param = torch.Tensor(param)
    return param

def block_orthonormal_initialization(dim_in, dim_out, num_blocks, factor=1.0, seed=None, dtype='float64'):
    param = torch.cat([orthonormal_initialization(dim_in, dim_out) for i in range(num_blocks)], 1)
    return param

if __name__ == '__main__':
    mod = RANLayer(3, 5, num_layers = 4, highway=True, tie_weights=True, use_tanh=False).cuda()
    x = pack_padded_sequence(Variable(torch.randn(4,7,3)).cuda(), [7,6,5,4], batch_first=True)
    h0 = Variable(torch.randn(4, 4, 5).cuda())
    c0 = Variable(torch.randn(4, 4, 5).cuda())
    output, hidden = mod(x, hx=(h0, c0))

    print (output)
