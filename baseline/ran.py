import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
from torch.nn.utils.rnn import PackedSequence
from torch.nn.utils.rnn import pack_padded_sequence
from torch.nn.utils.rnn import pad_packed_sequence

class RAN(nn.Module):
    def __init__(self, input_size, output_size, num_layers, recurrent_dropout_prob=0.0, bidirectional=False, use_hidden_for_gates=True, highway = False, output_gate=False, tie_weights=False, use_tanh=True):
        super(RAN, self).__init__()
        self.input_size = input_size
        self.output_size = output_size
        self.out_dim = output_size
        self.recurrent_dropout_prob = recurrent_dropout_prob
        self.num_layers = num_layers
        self.directions = 2 if bidirectional else 1
        self.use_hidden_for_gates = use_hidden_for_gates
        self.output_gate = output_gate
        self.highway = highway
        self.tie_weights = tie_weights
        self.use_tanh = use_tanh

        layers = []
        for i in range(num_layers):
            d_layers = []
            for d in range(self.directions):
                direction = 1 if d == 0 else -1
                d_str = "" if d == 0 else "_reverse"
                i_size = self.input_size if i==0 else self.directions * self.output_size
                layer = RANLayer(i_size, output_size, direction, recurrent_dropout_prob = recurrent_dropout_prob, use_hidden_for_gates = use_hidden_for_gates, output_gate=output_gate, highway = highway, tie_weights = tie_weights, use_tanh=use_tanh)
                d_layers.append(layer)
                setattr(self, 'layer_%d%s'%(i,d_str), layer)
            layers.append(d_layers)
        self.layers = layers

    def forward(self, input, hx=None, dropout_weights = None):
        if dropout_weights is not None:
            assert dropout_weights.size(0) == self.num_layers and dropout_weights.size(1) == self.directions, "dropout_weights wrong size: %s"%(str(dropout_weights.size()))
        curr = input
        hs = []
        cs = []
        for i, l in enumerate(self.layers):
            currs = []
            for j, dir_l in enumerate(l):
                if dropout_weights is not None:
                    d = dropout_weights[i,j]
                else:
                    d = None
                if hx:
                    h0 = hx[0][i*len(l) + j]
                    c0 = hx[1][i*len(l) + j]
                    hidden = (h0, c0)
                else:
                    hidden = None
                layer_curr, layer_hidden = dir_l(curr, hx=hidden, dropout_weights=d)
                currs.append(layer_curr)
                hs.append(layer_hidden[0])
                cs.append(layer_hidden[1])
            if len(currs) == 1:
                curr = currs[0]
            else:
                currs = [pad_packed_sequence(x, batch_first=True) for x in currs]
                l = currs[0][1]
                currs = [x[0] for x in currs]
                curr = torch.cat(currs, dim=2)
                curr = pack_padded_sequence(curr, l, batch_first=True)

        print (hs)
        hs = torch.stack(hs)
        cs = torch.stack(cs)

        return curr, (hs, cs)

def num_gates(output_gate, tie_weights, highway, use_hidden_for_gates):
    i_gates = 2
    h_gates = 1
    if not tie_weights:
        i_gates += 1
        h_gates += 1
    if output_gate:
        i_gates += 1
        h_gates += 1
    if highway:
        i_gates += 2
        h_gates += 1
    bias_gates = h_gates

    if not use_hidden_for_gates:
        h_gates = 0

    return i_gates, h_gates, bias_gates

class RANLayer(nn.Module):
    def __init__(self, input_size, output_size, direction, recurrent_dropout_prob = 0., use_hidden_for_gates=True, output_gate = False, highway = False, tie_weights=False, use_tanh=True):
        super(RANLayer, self).__init__()
        self.input_size = input_size
        self.output_size = output_size
        self.direction = direction
        self.recurrent_dropout_prob = recurrent_dropout_prob
        self.output_gate = output_gate
        self.highway = highway
        self.use_hidden_for_gates = use_hidden_for_gates
        self.highway = highway
        self.tie_weights = tie_weights
        self.use_tanh = use_tanh
        
        self.i_gates, self.h_gates, self.bias_gates = num_gates(output_gate, tie_weights, highway, use_hidden_for_gates)

        self.xlin = nn.Linear(input_size, self.i_gates * output_size, bias=False)
        if self.h_gates > 0:
            self.hlin = nn.Linear(output_size, self.h_gates * output_size, bias=False)
        self.bias = nn.Parameter(torch.randn(self.bias_gates * output_size))
            
    def forward(self, input, hx=None, dropout_weights=None):
        assert isinstance(input, PackedSequence), 'input must be PackedSequence but got %s'%(type(input))

        input, lengths = pad_packed_sequence(input, batch_first=True)

        B = input.size()[0]
        T = input.size()[1]

        assert B==len(lengths), 'T not equal len(lengths) (%d vs %d)'%(T, len(lengths))

        output_hs = Variable(input.data.new().resize_(B, T, self.output_size).fill_(0))
        if hx is not None:
            prev_h = hx[0]
            prev_c = hx[1]
        else:
            prev_c = Variable(input.data.new().resize_(B, self.output_size).fill_(0))
            prev_h = Variable(input.data.new().resize_(B, self.output_size).fill_(0))
        curr_len = 0
        curr_len_index = B-1 if self.direction == 1 else 0

        if self.recurrent_dropout_prob > 0 and dropout_weights is None:
            dropout_weights = Variable(input.data.new().resize_(B, self.output_size))
            torch.rand(dropout_weights.size(), out=dropout_weights.data)
            dropout_weights = dropout_weights > self.recurrent_dropout_prob
            dropout_weights = dropout_weights.float()

        for ii in range(T):
            ind = ii if self.direction == 1 else T-ii-1

            if self.direction == 1:
                while lengths[curr_len_index] <= ind:
                    curr_len_index -= 1
            elif self.direction == -1:
                while curr_len_index < len(lengths)-1 and lengths[curr_len_index+1] > ind:
                    curr_len_index += 1 

            c_prev = prev_c[0:curr_len_index+1].clone()
            h_prev = prev_h[0:curr_len_index+1].clone()
            x_ = input[0:curr_len_index+1,ind]

            x2g = self.xlin(x_) # B x T x 6*O
            if self.h_gates > 0:
                h2g = self.hlin(h_prev)

            x_ind = 0
            h_ind = 0
            bias_ind = 0
            os = self.output_size

            c_init = x2g[:,x_ind:x_ind+os]
            x_ind += os

            i = x2g[:,x_ind:x_ind+os]
            x_ind += os
            if self.h_gates > 0:
                i = i + h2g[:,h_ind:h_ind+os]
                h_ind += os
            i = i + self.bias[bias_ind:bias_ind+os]
            bias_ind += os
            i = F.sigmoid(i)

            if self.tie_weights:
                f = 1.-i
            else:
                f = x2g[:,x_ind:x_ind+os]
                x_ind += os
                if self.h_gates> 0:
                    f = f + h2g[:,h_ind:h_ind+os]
                    h_ind += os
                f = f + self.bias[bias_ind:bias_ind+os]
                bias_ind += os
                f = F.sigmoid(f)

            c = i*c_init + f*c_prev

            if self.use_tanh:
                h_prime = F.tanh(c)
            else:
                h_prime = c

            if self.output_gate:
                o = x2g[:,x_ind:x_ind+os]
                x_ind += os
                if self.h_gates > 0:
                    o = o + h2g[:,h_ind:h_ind+os]
                    h_ind += os
                o = o + self.bias[bias_ind:bias_ind+os]
                bias_ind += os
                o = F.sigmoid(o)
                h_prime = o * h_prime

            if self.highway:
                r = x2g[:,x_ind:x_ind+os]
                x_ind += os
                if self.h_gates > 0:
                    r = r + h2g[:,h_ind:h_ind+os]
                    h_ind += os
                r = r + self.bias[bias_ind:bias_ind+os]
                bias_ind += os
                r = F.sigmoid(r)
                h_hat = r*h_prime + (1 - r) * x2g[:,x_ind:x_ind+os]
            else:
                h_hat = h_prime

            if self.training and dropout_weights is not None:
                drop_w = dropout_weights[0:curr_len_index+1]
                h = h_hat * drop_w
            else:
                h = h_hat * (1 - self.recurrent_dropout_prob)

            prev_c = Variable(input.data.new().resize_(B, self.output_size).fill_(0))
            prev_h = Variable(input.data.new().resize_(B, self.output_size).fill_(0))
            prev_c[0:curr_len_index+1] = c
            prev_h[0:curr_len_index+1] = h
            output_hs[0:curr_len_index+1,ind] = h

        output_hs = pack_padded_sequence(output_hs, lengths, batch_first = True)
        return output_hs, (prev_h, prev_c)

if __name__ == '__main__':
    mod = RAN(3, 5, num_layers = 4, bidirectional=True).cuda()
    x = pack_padded_sequence(Variable(torch.randn(4,7,3)).cuda(), [7,6,5,4], batch_first=True)
    output = mod(x)

    print (output)
    
